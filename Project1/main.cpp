/*
	@Author: Angelina Sumakova
	@Group: AI-152
	@Date: 21.11.18
	@Algorithm: Kalyna
*/

#include <iostream>
#include <conio.h>
#include <cstdio>
#include <memory.h>
#include <string>
#include <algorithm>
#include <stdexcept>
#include <vector>
#include <sstream>
#include "kalyna.h"
#include "transformations.h"

using namespace std;

enum CipherChoice { 
	C22 = 1, 
	C24 = 2, 
	C44 = 3, 
	C48 = 4, 
	C88 = 5 
};

enum BYTES { 
	TWO		= 2, 
	FOUR	= 4, 
	EIGHT	= 8 
};

enum KalynaMode { 
	ENCIPHERING, 
	DECIPHERING 
};

const int LETTERS	= 6;
const int GARBAGE	= 999;
const int INIT		= 0;
int excessElements	= INIT;

std::string string_to_hex(const std::string& input);
uint64_t string_to_uint64(const std::string& input);
std::string hex_to_string(const std::string& input);
string uint64_to_string(uint64_t t);
vector<uint64_t>* kalina_cipher(CipherChoice choice, vector<uint64_t> input, KalynaMode mode);

uint64_t KEY22[2] = {
	0x0706050403020109ULL, 0x0f0e0d0c0a0a0908ULL
};

uint64_t KEY24[4] = {
	0x0706050123020105ULL, 0x0f0e240c0b0accc8ULL,
	0x1716151413121110ULL, 0x1f1e1d1c1b581918ULL
};

uint64_t KEY44[4] = {
	0x0706050403033100ULL, 0x0f0e0d0c041a0908ULL,
	0x1716151413121110ULL, 0x1f1e1d1c1b1a1918ULL
};

uint64_t KEY48[8] = {
	0x0706050403020100ULL, 0x0f0e0d0c0b0a1908ULL,
	0x1716151413121860ULL, 0x1f1f1d1c1b1a1918ULL,
	0x2726252463222120ULL, 0x2f2e2d2c6b2a29c8ULL,
	0x3731253433323130ULL, 0x3f3e3d3c3b3a3938ULL
};

uint64_t KEY88[8] = {
	0x0706050403020100ULL, 0x0f0e0d0c0b0a0908ULL,
	0x1726151413121110ULL, 0x1f1e1d1c1b1a1918ULL,
	0x2726255553222120ULL, 0x2f2e2d5e2b2a2928ULL,
	0x3736353433323130ULL, 0x3f3e3d3c3b3a3918ULL
};

int main(int argc, char** argv) {

	cout << "Please, enter text to encipher with Kalyna algorithm: " << endl;

	string s;
	getline(cin, s);

	cout << endl << "Choose Kalyna algorithm's type: ";
	cout << endl << "1) 128 bit, 128 bit";
	cout << endl << "2) 128 bit, 256 bit";
	cout << endl << "3) 256 bit, 256 bit";
	cout << endl << "4) 256 bit, 512 bit";
	cout << endl << "5) 512 bit, 512 bit";
	cout << endl << "Answer: ";

	int answer = INIT;
	string s1;
	getline(cin, s1);

	try {
		answer = stoi(s1);

		while (answer < 1 || answer > 5) {
			cout << endl << "Please enter correct value in range [1:5]!" << endl << "Answer: ";
			getline(cin, s1);
			answer = stoi(s1);
		}
	}
	catch (exception e) {
		cout << endl << "Incorrect input! Selected by default: 1) 128 bit, 128 bit" << endl;
		answer = 1;
	}

	CipherChoice choice = static_cast<CipherChoice>(answer);

	cout << endl << "Plain text to encipher: " << s << endl;
	
	int textLen = s.length();
	int lastLetters = 0;

	if (textLen % LETTERS != 0) {
		lastLetters = LETTERS - (textLen % LETTERS);

		for (int i = 0; i < lastLetters; i++) {
			s.append(" ");
		}

		textLen = s.length();
		lastLetters = textLen % LETTERS;
	}

	vector<string> string_text;
	vector<uint64_t> hex_text;

	for (int i = 0, j = 0; j < textLen/LETTERS; i += LETTERS, j++) {
		string tmp;
		tmp = string_to_hex(s.substr(i, LETTERS));
		string_text.push_back(tmp);
	}

	for (int i = 0; i < string_text.size(); i++) {
		hex_text.push_back(string_to_uint64(string_text.at(i)));
	}

	vector<uint64_t>* cipheredText = kalina_cipher(choice, hex_text, ENCIPHERING);
	vector<uint64_t>* decipheredText = kalina_cipher(choice, *cipheredText, DECIPHERING);

	cout << endl;

	string cipherText;
	for (int i = 0; i < cipheredText->size(); i++) {
		if (i % 2 == 0) {
			cipherText.append("\n");
		}

		cipherText.append(uint64_to_string(cipheredText->at(i)));
	}
	cout << "Ciphertext: " << endl;
	cout << cipherText << endl;


	string decipherHexText;
	for (int i = 0; i < decipheredText->size(); i++) {
		if (i % 2 == 0) {
			decipherHexText.append("\n");
		}

		decipherHexText.append(uint64_to_string(decipheredText->at(i)));
	}
	cout << endl << "Deciphered hex representation: " << endl;
	cout << decipherHexText << endl;


	string decipherText;
	for (int i = 0; i < decipheredText->size(); i++) {
		string t = uint64_to_string(decipheredText->at(i));
		t = hex_to_string(t);
		decipherText.append(t);
	}
	cout << endl << "Deciphered text: " << endl;
	cout << decipherText << endl;

	delete cipheredText;
	delete decipheredText;

	system("pause");
	return 0;
}

string uint64_to_string(uint64_t t)
{
	ostringstream ss;
	ss << hex << t;
	string tmp = ss.str();
	std::transform(tmp.begin(), tmp.end(), tmp.begin(), ::toupper);
	
	return tmp;
}

vector<uint64_t>* kalina_cipher(CipherChoice choice, vector<uint64_t> input, KalynaMode mode) {

	vector<uint64_t> * result = new vector<uint64_t>();
	kalyna_t * ctx = nullptr;
	uint64_t * key = nullptr;
	int blocksPerRound = INIT;

	switch (choice) {
	case C22:
		blocksPerRound = TWO;
		key = KEY22;
		break;
	case C24:
		blocksPerRound = TWO;
		key = KEY24;
		break;
	case C44:
		blocksPerRound = FOUR;
		key = KEY44;
		break;
	case C48:
		blocksPerRound = FOUR;
		key = KEY48;
		break;
	case C88:
		blocksPerRound = EIGHT;
		key = KEY88;
		break;
	}
	
	int rounds = input.size() / blocksPerRound;
	int pushedBlocks = 0;

	while (input.size() % blocksPerRound != 0) {
		input.push_back(GARBAGE);
		excessElements++;
		rounds = input.size() / blocksPerRound;
	}

	vector<uint64_t> inputText;
	for (int i = 0; i < rounds; i++) {

		uint64_t * returnText = nullptr;

		for (int j = 0; j < blocksPerRound; j++) {
			inputText.push_back(input.at(pushedBlocks));
			pushedBlocks++;
		}

		switch (choice) {
		case C22:
			ctx = KalynaInit(128, 128); 
			returnText = new uint64_t[2];
			break;
		case C24:
			ctx = KalynaInit(128, 256);
			returnText = new uint64_t[2];
			break;
		case C44:
			ctx = KalynaInit(256, 256);
			returnText = new uint64_t[4];
			break;
		case C48:
			ctx = KalynaInit(256, 512);
			returnText = new uint64_t[4];
			break;
		case C88:
			ctx = KalynaInit(512, 512);
			returnText = new uint64_t[8];
			break;
		}

		KalynaKeyExpand(key, ctx);
		
		switch (mode) {
		case ENCIPHERING:
			KalynaEncipher(&inputText[i*blocksPerRound], ctx, returnText);
			break;
		case DECIPHERING:
			KalynaDecipher(&inputText[i*blocksPerRound], ctx, returnText);
			break;
		}

		for (int k = 0; k < blocksPerRound; k++) {
			result->push_back(returnText[k]);
		}

		delete returnText;
	}

	if (mode == DECIPHERING) {
		for (int i = 0; i < excessElements; i++) {
			result->pop_back();
		}
	}

	KalynaDelete(ctx);
	return result;
}

std::string string_to_hex(const std::string& input) {
	static const char* const lut = "0123456789ABCDEF";
	size_t len = input.length();

	std::string output;
	output.reserve(2 * len);
	for (size_t i = 0; i < len; ++i) {
		const unsigned char c = input[i];
		output.push_back(lut[c >> 4]);
		output.push_back(lut[c & 15]);
	}

	return output;
}

uint64_t string_to_uint64(const std::string& input) {
	return stoull(input, 0, 16);
}

std::string hex_to_string(const std::string& input) {
	static const char* const lut = "0123456789ABCDEF";
	size_t len = input.length();
	if (len & 1) {
		throw std::invalid_argument("odd length");
	}

	std::string output;
	output.reserve(len / 2);
	for (size_t i = 0; i < len; i += 2) {
		char a = input[i];
		const char* p = std::lower_bound(lut, lut + 16, a);
		if (*p != a) {
			throw std::invalid_argument("not a hex digit");
		}

		char b = input[i + 1];
		const char* q = std::lower_bound(lut, lut + 16, b);
		if (*q != b) {
			throw std::invalid_argument("not a hex digit");
		}

		output.push_back(((p - lut) << 4) | (q - lut));
	}
	return output;
}